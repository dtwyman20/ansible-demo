FROM alpine:latest

RUN apk add --no-cache bash

COPY echo.sh /scripts/echo.sh

CMD ["bash", "/scripts/echo.sh"]

