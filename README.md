# Ansible/Docker Demo

Ansible and Docker Demonstration

To execute this demonstration perform the following:

    Download the ans-demo.yml and ans1.tar file onto a system the has docker and ansible installed

    Execute "docker load -i ans1.tar". This will load the docker image into the docker repository

    Execute "ansible-playbook ans-demo.yml". This will start the docker container and will display the output

The docker image is a customized image based upon Alpine. The Alpine image is considerably smaller in size than a 
comparable image based upon CentOS 8. The echo.sh script is copied into the image during
the build process.
